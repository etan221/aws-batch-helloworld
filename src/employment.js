'use strict';
// Libraries
const AWS = require('aws-sdk');
const parameterStore = new AWS.SSM();
const fetch = require("node-fetch");

const {
    getPersonId,
    getTodayFromStageVar,
} = require("./helpers/commonHelper");
const {
    buildResponse,
    ERROR_CONNECT_COGNITO,
} = require("./helpers/responseHelper");
const { 
  getEmploymentUpi 
} = require("./api/employmentApi");

const IS_PROD = process.env.CURRENT_ENVIRONMENT === "prod";
console.log(process);
console.log(`process.env: ${process.env}`);

let args = process.argv.slice(2);
let paramUpi = args[0];
console.log(`paramUpi: ${JSON.stringify(paramUpi)}`);
let result = getEmploymentUpi(paramUpi);
console.log(`result: ${result}`);