const utils = require('@uoa/utilities');
const COGNITO_DOMAIN = process.env.COGNITO_DOMAIN_NAME + '.auth.' + process.env.COGNITO_REGION + '.amazoncognito.com';

module.exports.getPersonIdFromCognito = async function (event) {
    let userInfo;
    try {
        userInfo = await utils.getUserInfo(event, COGNITO_DOMAIN);
    } catch (err) {
        throw new Error(`Connect cognito: ${COGNITO_DOMAIN}, (cause=${err})`);
    }
    if (userInfo.error) {
        throw new Error(`User not found (cause=${userInfo.error})`);
    }
    return { empId: userInfo['custom:EmpID'], name: userInfo.name, email: userInfo.email };
}