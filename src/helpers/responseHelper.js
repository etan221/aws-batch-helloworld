module.exports.buildResponse = function (status, body) {
    console.log(`status: ${status} body: ${JSON.stringify(body)}`)
    return {
        statusCode: status,
        body: JSON.stringify(body),
        headers: {
            "Access-Control-Allow-Origin": process.env.CORS_ACCESS_CONTROL_ALLOW_ORIGINS
        }
    };
}

module.exports.USER_NOT_FOUND = { 'message': 'User not found' };
module.exports.ERROR_CONNECT_COGNITO = { 'message': 'Error getting user from cognito' };
module.exports.ERROR_RETRIEVE_DATA = { 'message': 'Error retrieving data' };