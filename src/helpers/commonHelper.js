const moment = require('moment-timezone');
const { getPersonIdFromCognito } = require('./cognitoHelper');

module.exports.getPersonId = async function (event, isProd) {
    let personId = getPersonIdFromStageVar(event, isProd);
    if (!personId) {
        personId = await getPersonIdFromCognito(event);
    }
    return personId;
}

function getPersonIdFromStageVar(event, isProd) {
    let personId;
    if (event['stageVariables'] && !isProd) {
        if (event['stageVariables']['personId']) {
            personId = event['stageVariables']['personId'];
            console.info(`TESTING-ONLY - Overridden personId with ${personId}`);
        }
    }
    return personId;
}

module.exports.getTodayFromStageVar = function (event, isProd) {
    let today = moment.tz('Pacific/Auckland').format('YYYY-MM-DD');   // default
    if (event['stageVariables'] && !isProd) { // from stage variable
        if (event['stageVariables']['date']) {
            today = event['stageVariables']['date'];
            console.info(`TESTING-ONLY - Overridden date with ${today}`);
        }
    }
    return today;
}

module.exports.orderByModifiedAt = function (a, b) {
    if (a.modifiedAt > b.modifiedAt) {
        return -1;
    }
    return 1;
}

module.exports.orderByCostId = function (a, b) {
    if (a.costId > b.costId) {
        return -1;
    }
    return 1;
}