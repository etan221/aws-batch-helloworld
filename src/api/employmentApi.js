'use strict';
// Libraries
const AWS = require('aws-sdk');
const parameterStore = new AWS.SSM();
const fetch = require("node-fetch");

module.exports.getEmploymentUpi = async function (upiOrId) {
  let result = null;
  try {
    console.log(`querying employment api..${upiOrId}`);
    console.log(`EMPLOYMENT_APIKEY_SSM_PATH: ${process.env.EMPLOYMENT_APIKEY_SSM_PATH}`);
    console.log(`IDENTITY_APIKEY_SSM_PATH: ${process.env.IDENTITY_APIKEY_SSM_PATH}`);

    // const apiKeySecret = await parameterStore.getParameter({ Name: process.env.IDENTITY_APIKEY_SSM_PATH, WithDecryption: true }).promise();
    // const apiKeySecret = await parameterStore.getParameter({ Name: process.env.EMPLOYMENT_APIKEY_SSM_PATH, WithDecryption: true }).promise();
      
    // console.log(`apiKeySecret: ${JSON.stringify(apiKeySecret)}`);
    const tmpApiKey = 'dce7e6bf153f42d28363d94a12e0079d';
    // let params = "firstname=Eric&from=0&lastname=Tan&size=100";
    // let params = "firstname=" + searchStr + "&from=0&size=200";        
    
    let EMPLOYMENT_API = 'https://apis.test.auckland.ac.nz/employment/v1'
    // let url = `${process.env.EMPLOYMENT_API}/employee/${upiOrId}`;
    let url = `${EMPLOYMENT_API}/employee/${upiOrId}`;
    console.log(`url: ${url}`);
    const response = await fetch(`${url}`, { accept: "application/json", headers: { "apikey" : tmpApiKey } });
    // const response = await fetch(`${url}`, { accept: "application/json", headers: { "apikey" : apiKeySecret.Parameter.Value } });
    // const result = await fetch(`${process.env.GROUPMEMBERSHIP_API}/person?${upi}`, { accept: "application/json", headers: { "apikey" : apiKeySecret.Parameter.Value } });
    // console.log(`groupMembership result: ${JSON.stringify(result)}`);
    if (!response.ok) {
        throw new Error('error querying employment api: ' + JSON.stringify(response));
    }
    const json = await response.json();
    console.log(`json:`, json);
    result = json;

    // if (json && json.length > 0){
    //   // console.log(`json: ${json}`);
    //   for (let i = 0; i < json.length; i++){
    //     // console.log(`json[${i}]: ${json[i]}`);
    //     if (json[i].indexOf('ResearchBudget') !== -1){
    //       // json[0]: CN=TOOL_ADMIN_users,OU=ResearchBudget,OU=app,OU=Groups,DC=uoatest,DC=auckland,DC=ac,DC=nz
    //       let adDomain = json[i].split(',');
    //       for (let element of adDomain){
    //         // console.log(`element: ${element}`)
    //         let pair = element.split('=');         
    //         if (pair[1].indexOf('_users') !== -1){
    //           // results.push(pair[1]);
    //           // console.log(`pair[1]: ${pair[1]}`);
    //           // console.log(`put: ${pair[1].slice(0,pair[1].indexOf('_users'))}`);
    //           // push('TOOL_ADMIN')
    //           results.push(pair[1].slice(0,pair[1].indexOf('_users')));
    //         }
    //       }
    //     }
    //   }
    // }
        
  } catch (error) {
    console.log(error);
    return;
  }
  // results.sort();
  console.log('result : ', JSON.stringify(result));
  return result;
}

