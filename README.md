# README #

Project objectives:
- to create an docker image that calls API and return values
- to push the docker image to AWS Batch

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


* Configuration: `npm install`
* Dependencies
* Database configuration
* How to run tests: `npm run start-api`
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

###  AWS Batch
- [AWS batch | Docker | ECR | Python | Jobqueue](https://www.youtube.com/watch?v=EoC46x0jAas)
- [AWS Batch Demo - Part 1](https://www.youtube.com/watch?v=-gX9Sr6fdVc)
- [AWS Batch Demo - Part 2](https://www.youtube.com/watch?v=HTtOJn2RHqA)

### [Docker-izing a NodeJS ExpressJS API - Tutorial](https://www.youtube.com/watch?v=CsWoMpK3EtE)

|Action| Command
|:---|:---
|1. Install the dependencies to folder node_modules |npm install
|2. run locally|npm run start + browser: http://localhost:3000/
|3. build docker image| `docker build -t node-docker-tutorial .`
|4. run docker| `docker run -it -p 9000:3000 node-docker-tutorial`
|5. list docker | `docker ps`


```
$ docker build -t node-docker-tutorial .
[+] Building 3.8s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 => => transferring dockerfile: 269B
 => [internal] load .dockerignore
 => => transferring context: 2B
 => [internal] load metadata for docker.io/library/node:9-slim
 => [internal] load build context
 => => transferring context: 23.91kB
 => [1/5] FROM docker.io/library/node:9-slim@sha256:288b29c1925d65b2d7d8701f8ada201e7dcd066438c0fb4299c35dff129b893f
 => CACHED [2/5] WORKDIR /app
 => CACHED [3/5] COPY package.json ./app
 => CACHED [4/5] RUN npm install
 => [5/5] COPY . /app
 => exporting to image
 => => exporting layers
 => => writing image sha256:a99e28e263033a2805c7a693e86e320ce0af5c8c07a936602ce7cb50e90577db
 => => naming to docker.io/library/node-docker-tutorial

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
```

```
$ docker run -it -p 9000:3000 node-docker-tutorial

> aws-batch-helloworld@1.0.0 start /app
> node app.js
```

```
$ docker ps
CONTAINER ID   IMAGE                  COMMAND       CREATED          STATUS          PORTS                    NAMES        
da17348c3549   node-docker-tutorial   "npm start"   11 seconds ago   Up 10 seconds   0.0.0.0:9000->3000/tcp   serene_jepsen
```


### [Create, Push & Store Container Images in ECR](https://www.youtube.com/watch?v=Yy9AGt4m0_I)

[create repo on aws - web mode](https://youtu.be/Yy9AGt4m0_I?t=29)

- aws account: `devops/etan221@uoa-sandbox`
- private repo for image: `416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld`

[create repo on aws - cmd mode](https://youtu.be/Yy9AGt4m0_I?t=68)
- `aws ecr create-repository --repository-name aws-batch-helloworld --region ap-southeast-2 --profile saml`

```
> aws ecr create-repository --repository-name aws-batch-helloworld --region ap-southeast-2 --profile saml

{
    "repository": {
        "repositoryArn": "arn:aws:ecr:ap-southeast-2:416527880812:repository/aws-batch-helloworld",
        "registryId": "416527880812",
        "repositoryName": "aws-batch-helloworld",
        "repositoryUri": "416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld",
        "createdAt": 1640129560.0,
        "imageTagMutability": "MUTABLE",
        "imageScanningConfiguration": {
            "scanOnPush": false
        },
        "encryptionConfiguration": {
            "encryptionType": "AES256"
        }
    }
}
```

### [Push image to ECR](https://youtu.be/Yy9AGt4m0_I?t=169)

|Action on image| NPM cmd | Actual Cmd
|:---|:---|:---
|1. Get access |`npm run cred`|`python C:\\opt\\uoa_project\\utility-aws-cli-access\\aws_saml_login.py --idp iam.auckland.ac.nz --account \"UoA Sandbox - devops\" --profile saml  --user etan221"`
|2. aws create repository|`npm run ecr-create`|`aws ecr create-repository --repository-name aws-batch-helloworld --region ap-southeast-2 --profile saml`
|3. aws describe repository| `npm run ecr-desc`|`aws ecr describe-repositories --region ap-southeast-2 --profile saml`
|4. docker list images| `npm run dkr-img`|`docker images`
|5. docker tag images | `npm run dkr-tag` | `docker tag 08fcbd701e44 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld:latest`
|6. login ECR| `npm run ecr-login`|`aws ecr get-login-password --region ap-southeast-2 --profile saml | docker login --username AWS --password-stdin 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com`,
|7. docker push image| `npm run dkr-push`|`docker push 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld:latest`

```
$ docker tag 6ce89fa8692d 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld:latest
```

```
$ npm run ecr-login

> aws-batch-helloworld@1.0.0 ecr-login
> aws ecr get-login-password --region ap-southeast-2 --profile saml | docker login --username AWS --password-stdin 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com

Login Succeeded

Logging in with your password grants your terminal complete access to your account.
For better security, log in with a limited-privilege personal access token. Learn more at https://docs.docker.com/go/access-tokens/
```

```
$ npm run dkr-push

> aws-batch-helloworld@1.0.0 dkr-push
> docker push 416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld:latest

The push refers to repository [416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld]
571646871625: Pushed
6e9151809476: Pushed
a2c5d4914db5: Pushed
37c175f68524: Pushed
bfb7ac42eb6e: Pushed
1a058d5342cc: Pushed
latest: digest: sha256:be5987530e3785893d90709295b24d32c1eed4e64360f99ec8cd9539103d134d size: 1578
```

```
$ docker images
REPOSITORY                                                               TAG       IMAGE ID       CREATED          SIZE
416527880812.dkr.ecr.ap-southeast-2.amazonaws.com/aws-batch-helloworld   latest    6ce89fa8692d   46 minutes ago   153MB
aws-batch-helloworld                                                     latest    6ce89fa8692d   46 minutes ago   153MB
integration-hr-elements-batch                                            latest    dc597d3a365d   20 hours ago     671MB
```


### [Pass parameter to node](https://stackoverflow.com/questions/4351521/how-do-i-pass-command-line-arguments-to-a-node-js-program)


- [Terraform with AWS](https://www.youtube.com/watch?v=XxTcw7UTues)

- [node read yaml](https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-node-js-javascript/)


### Jan 5: Planning
x 1. node api.js -> employment api -> response json
x 2. node api.js -> employment api -> *** docker image -> response json
x 3. *** tsc -> node api.js -> employment api -> docker image -> response json
4. node api.js -> employment api -> *** access aws role-tst -> docker image -> response json
5. node api.js -> employment api -> *** read yaml -> access aws role-tst -> docker image -> response json


### [add typescript](https://www.section.io/engineering-education/how-to-use-typescript-with-nodejs/)
- `node src/app.ts` would works, if app.ts contain javascript only; `node src/app.ts` would NOT works, if app.ts contain typescript e.g. `import { HandleService } from './handle-service'`
- then require `tsc` to transpile to `.build/src/app.js` in `ES20202` then run `node .build/src/app.js` will works.

|Action on image| NPM cmd | Actual Cmd
|:---|:---|:---
|1. Compile typescript to javascript | null |`tsc`