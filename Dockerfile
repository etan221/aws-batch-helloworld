FROM alpine:3.14
RUN apk add --update nodejs
RUN apk add --update npm
COPY .build/src/. .build/src/.
COPY package.json .
RUN npm install
CMD ["node", ".build/src/employment.js", "misl938"]